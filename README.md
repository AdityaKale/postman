As a tester, we used Postman to perform various tasks to ensure the quality and functionality of APIs.
Postman allows you to create and send various types of HTTP requests, including GET, POST, PUT, DELETE, and PATCH.
we scriped environment using JavaScript that allows you to write tests for our API requests. we  checked responses for specific content, status codes, headers, and more.
Collections: we organized requests into collections, making it easy to manage and execute multiple API requests as part of a workflow.
we did automation through the  Newman (a command-line collection runner). This is useful for running collections of requests and tests automatically.
